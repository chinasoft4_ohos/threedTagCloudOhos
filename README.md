# 3dTagCloudOhos 

## 项目介绍
- 项目名称：3dTagCloudOhos 
- 所属系列：openharmony的第三方组件适配移植
- 功能：TagCloudView是一个基于ViewGroup实现的控件，支持将一组View展示为一个3D球形集合，并支持全方向滚动。
- 项目移植状态：主功能完成
- 调用差异：无差异
- 开发版本：sdk6,DevEco Studio 2.2 Beta1
- 基线版本：Release 1.2.0
## 效果演示
<img src="img/demo1.gif"></img>
<img src="img/demo2.gif"></img>
## 安装教程
1.在项目根目录下的build.gradle文件中

```

allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}

```
2.在entry模块的build.gradle文件中，
```
dependencies {
    implementation('com.gitee.chinasoft_ohos:3dTagCloudOhos:1.0.0')
    ......
 }

 ```
 在sdk6,DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件, 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```
- 在布局文件中引入  
```  
<com.moxun.tagcloudlib.view.TagCloudView/>  
```  

- 设置Adapter    
继承`TagsAdapter`，实现以下方法
  
    **public int getCount();**  
*返回Tag数量*  
**public View getView(Context context, int position, ComponentContainer parent);**  
*返回每个Tag实例*  
**public Object getItem(int position);**  
*返回Tag数据*  
**public int getPopularity(int position);**  
*针对每个Tag返回一个权重值，该值与ThemeColor和Tag初始大小有关；一个简单的权重值生成方式是对一个数N取余或使用随机数*  
**public void onThemeColorChanged(Component component,int themeColor);**  
*Tag主题色发生变化时会回调该方法*  
  
```

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
* 1.0.0

## 版权和许可信息
```
 The MIT License (MIT)
 Copyright © 2016 moxun

 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the “Software”),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.