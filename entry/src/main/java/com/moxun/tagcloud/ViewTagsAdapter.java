package com.moxun.tagcloud;

import com.moxun.tagcloudlib.view.ColorUtils;
import com.moxun.tagcloudlib.view.TagsAdapter;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by moxun on 16/3/4.
 */
public class ViewTagsAdapter extends TagsAdapter {
    @Override
    public int getCount() {
        return 20;
    }

    @Override
    public Component getView(Context context, int position, ComponentContainer parent) {
        Component view = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_tag_item_view, parent, false);
        return view;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public int getPopularity(int position) {
        return position % 5;
    }

    @Override
    public void onThemeColorChanged(Component view, int themeColor, float alpha) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(ColorUtils.redInt(themeColor),ColorUtils.greenInt(themeColor),ColorUtils.blueInt(themeColor),(int)(alpha * 255)));
        Component component = view.findComponentById(ResourceTable.Id_ohos_eye);
        component.setBackground(shapeElement);

        int color = Color.argb((int) (1 - alpha), 255, 255, 255);
        ImageUtils.setImageTint(new ShapeElement(), color, BlendMode.SRC_ATOP, ((Image) view.findComponentById(ResourceTable.Id_iv)));
    }
}
