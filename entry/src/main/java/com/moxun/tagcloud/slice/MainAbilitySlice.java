/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moxun.tagcloud.slice;

import com.moxun.tagcloud.*;
import com.moxun.tagcloudlib.view.TagCloudView;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice {
    private TagCloudView tagCloudView;
    private TextTagsAdapter textTagsAdapter;
    private ViewTagsAdapter viewTagsAdapter;
    private VectorTagsAdapter vectorTagsAdapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        tagCloudView = (TagCloudView) findComponentById(ResourceTable.Id_tag_cloud);

        textTagsAdapter = new TextTagsAdapter(new String[20]);
        viewTagsAdapter = new ViewTagsAdapter();
        vectorTagsAdapter = new VectorTagsAdapter(this);

        tagCloudView.setAdapter(textTagsAdapter);
        findComponentById(ResourceTable.Id_tag_text).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                tagCloudView.setAdapter(textTagsAdapter);
            }
        });

        findComponentById(ResourceTable.Id_tag_view).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                tagCloudView.setGetViewAdapter(viewTagsAdapter);
            }
        });

        findComponentById(ResourceTable.Id_tag_vector).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                tagCloudView.setAdapter(vectorTagsAdapter);
            }
        });
        findComponentById(ResourceTable.Id_test_fragment).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                nextPage(FractionTestAbility.class);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * nextPage
     *
     * @param pageName
     */
    public void nextPage(Class pageName) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(this.getBundleName())
                .withAbilityName(pageName.getName())
                .build();
        secondIntent.setOperation(operation);
        startAbility(secondIntent);
    }
}
