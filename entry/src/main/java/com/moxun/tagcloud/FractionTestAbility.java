package com.moxun.tagcloud;

import com.moxun.tagcloud.slice.FractionTestAbilitySlice;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class FractionTestAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(FractionTestAbilitySlice.class.getName());

    }
}
