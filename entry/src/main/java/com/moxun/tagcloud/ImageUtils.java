/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moxun.tagcloud;

import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;

public class ImageUtils {

    public static void setImageTint(Element element, int intColor, BlendMode mode, Image image){
        int[] colors=new int[]{intColor,intColor};
        int[][] states=new int[2][];
        states[0]=new int[]{0};
        states[1]=new int[]{0};
        element.setStateColorList(states,colors);
        element.setStateColorMode(mode);
        image.setImageElement(element);
    }
}
