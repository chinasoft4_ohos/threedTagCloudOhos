package com.moxun.tagcloud;

import com.moxun.tagcloudlib.view.ColorUtils;
import com.moxun.tagcloudlib.view.TagsAdapter;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by moxun on 16/1/19.
 */
public class TextTagsAdapter extends TagsAdapter {

    private List<String> dataSet = new ArrayList<>();

    public TextTagsAdapter(String... data) {
        dataSet.clear();
        Collections.addAll(dataSet, data);
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Component getView(final Context context, final int position, ComponentContainer parent) {
        Text tv = new Text(context);
        tv.setText("No." + position);
        tv.setTextAlignment(TextAlignment.CENTER);
        tv.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(context).setText("Tag" + position + "clicked").show();
            }
        });
        tv.setTextColor(Color.WHITE);
        tv.setTextAlignment(TextAlignment.CENTER);
        return tv;
    }

    @Override
    public Object getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public int getPopularity(int position) {
        return position % 7;
    }

    @Override
    public void onThemeColorChanged(Component view, int themeColor, float alpha) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(ColorUtils.redInt(themeColor), ColorUtils.greenInt(themeColor), ColorUtils.blueInt(themeColor), (int) (alpha * 255)));
        view.setBackground(shapeElement);
    }
}
