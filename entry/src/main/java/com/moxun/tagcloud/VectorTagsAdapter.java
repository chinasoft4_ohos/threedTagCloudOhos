package com.moxun.tagcloud;

import com.moxun.tagcloudlib.view.TagsAdapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.VectorElement;
import ohos.agp.render.BlendMode;
import ohos.app.Context;

/**
 * Created by moxun on 16/3/11.
 */
public class VectorTagsAdapter extends TagsAdapter {
    private Context context;
    public VectorTagsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 20;
    }

    @Override
    public Component getView(Context context, int position, ComponentContainer parent) {
        return LayoutScatter.getInstance(context).parse(ResourceTable.Layout_tag_item_vector, parent, false);
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public int getPopularity(int position) {
        return position % 5;
    }

    @Override
    public void onThemeColorChanged(Component view, int themeColor, float alpha) {
        Image imageView = (Image) view.findComponentById(ResourceTable.Id_vector_img);
        if (imageView == null) {
            return;
        }
        VectorElement shapeElement = new VectorElement(context, ResourceTable.Graphic_vector_drawable_iconfont_xingxing);
        shapeElement.setAntiAlias(true);
        ImageUtils.setImageTint(shapeElement, themeColor, BlendMode.SRC_ATOP, imageView);
    }
}
