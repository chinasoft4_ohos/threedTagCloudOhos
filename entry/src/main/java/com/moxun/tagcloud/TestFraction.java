package com.moxun.tagcloud;

import com.moxun.tagcloudlib.view.TagCloudView;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class TestFraction extends Fraction {
    private Component rootView;
    private TagCloudView fragmentTagcloud;

    public static TestFraction newInstance() {
        TestFraction fragment = new TestFraction();
        return fragment;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        rootView = scatter.parse(ResourceTable.Layout_test_fraction, null, false);
        instantiationViews();

        return rootView;
    }

    private void instantiationViews() {
        fragmentTagcloud = (TagCloudView) rootView.findComponentById(ResourceTable.Id_fragment_tagcloud);
        TextTagsAdapter adapter = new TextTagsAdapter(new String[20]);
        fragmentTagcloud.setAdapter(adapter);
}
}
