package com.moxun.tagcloudlib.view.graphics;

public class Point3DF {
  public float x;
  public float y;
  public float z;

  /**
   * Point3DF构造
   */
  public Point3DF() {}

  /**
   *
   * Point3DF构造
   *
   * @param x x
   * @param y y
   * @param z z
   */
  public Point3DF(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   *
   * Point3DF构造
   *
   * @param p Point3DF
   */
  public Point3DF(Point3DF p) {
    this.x = p.x;
    this.y = p.y;
    this.z = p.z;
  }

  /**
   *
   * Set the point's x and y coordinates
   *
   * @param x x
   * @param y y
   * @param z z
   */
  public final void set(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   *
   * Set the point's x and y coordinates to the coordinates of p
   *
   * @param p Point3DF
   */
  public final void set(Point3DF p) {
    this.x = p.x;
    this.y = p.y;
    this.z = p.z;
  }

  /**
   * negate
   */
  public final void negate() {
    x = -x;
    y = -y;
    z = -z;
  }

  /**
   * offset
   *
   * @param dx dx
   * @param dy dy
   * @param dz dz
   */
  public final void offset(float dx, float dy, float dz) {
    x += dx;
    y += dy;
    z += dz;
  }

  /**
   *
   * Returns true if the point's coordinates equal (x,y)
   *
   * @param x x
   * @param y y
   * @param z z
   * @return boolean true or false
   */
  public final boolean equals(float x, float y, float z) {
    return this.x == x && this.y == y && this.z == z;
  }

  @Override
  public String toString() {
    return "Point3DF(" + x + ", " + y + ", " + z + ")";
  }
}
