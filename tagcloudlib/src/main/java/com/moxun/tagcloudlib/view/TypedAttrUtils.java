/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moxun.tagcloudlib.view;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

/**
 * TypedAttrUtils
 *
 * @since 2021-03-29
 */
public class TypedAttrUtils {
    static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TypedAttrUtils");

    /**
     * isHaveAttr
     *
     * @param attrs attrs
     * @param attrName attrName
     * @return boolean true or false
     */
    public static boolean isHaveAttr(AttrSet attrs, String attrName){
        Attr attr = attrNoSuchElement(attrs, attrName);
        return attr != null;
    }

    /**
     * getIntColor
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int int
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    /**
     * getColor
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return Color Color
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /**
     * getBoolean
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return boolean true or false
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /**
     * getString
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return String String
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * getFloat
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return float float
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    /**
     * getInteger
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return getInteger getInteger
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getDimensionPixelSize
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int int
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int int
     */
    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            HiLog.info(LOG_LABEL, "attr.getDimensionValue() = " + attr.getDimensionValue());
            return attr.getDimensionValue();
        }
    }

    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
            HiLog.info(LOG_LABEL, "Exception = " + e.toString());
        }
        return attr;
    }
}
