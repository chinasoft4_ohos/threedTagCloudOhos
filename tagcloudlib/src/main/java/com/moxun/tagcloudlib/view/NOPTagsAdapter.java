package com.moxun.tagcloudlib.view;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * Default adapter and do nothing
 * Created by moxun on 16/3/25.
 */
/*package*/ class NOPTagsAdapter extends TagsAdapter {
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Component getView(Context context, int position, ComponentContainer parent) {
        return null;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public int getPopularity(int position) {
        return 0;
    }

    @Override
    public void onThemeColorChanged(Component view, int themeColor, float alpha) {

    }
}
