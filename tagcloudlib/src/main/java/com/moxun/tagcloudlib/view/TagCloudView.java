package com.moxun.tagcloudlib.view;

/**
 * Copyright © 2016 moxun
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.geo.Point;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class TagCloudView extends ComponentContainer implements TagsAdapter.OnDataSetChangeListener, Component.TouchEventListener
        , Component.EstimateSizeListener, ComponentContainer.ArrangeListener, Component.BindStateChangedListener {
    private static final HiLogLabel LOG_LAB = new HiLogLabel(0, 0, "TagCloudView");
    private static final float TOUCH_SCALE_FACTOR = 0.8f;
    private float mSpeed = 2f;
    private TagCloud mTagCloud;
    private float mInertiaX = 0.5f;
    private float mInertiaY = 0.5f;
    private float mCenterX, mCenterY;
    private float mRadius;
    private float mRadiusPercent = 0.9f;
    private boolean isGetView = false;

    private float[] mDarkColor = new float[]{1f, 0f, 0f, 1f};//rgba
    private float[] mLightColor = new float[]{0.9412f, 0.7686f, 0.2f, 1f};//rgba

    public TagCloudView(Context context) {
        super(context);
        init(context, null);
    }

    public TagCloudView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    public TagCloudView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }
    public enum MODE {
        MODE_DISABLE,
        MODE_DECELERATE,
        MODE_UNIFORM;
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface Mode {

    }
    public static final int MODE_DISABLE = 0;
    public static final int MODE_DECELERATE = 1;
    public static final int MODE_UNIFORM = 2;
    private boolean manualScroll;
    public int mMode;

    private int mMinSize;

    private boolean mIsOnTouch = false;
    private EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());
    private TagsAdapter mAdapter = new NOPTagsAdapter();
    private OnTagClickListener mOnTagClickListener;

    private void init(Context context, AttrSet attrs) {
        setTouchEventListener(this);
        setEstimateSizeListener(this);
        setTouchFocusable(true);
        setArrangeListener(this::onArrange);
        setBindStateChangedListener(this);
        //setFocusableInTouchMode(true); TODO
        mTagCloud = new TagCloud();
        if (attrs != null) {
            String m = TypedAttrUtils.getString(attrs, "autoScrollMode", "");
            if (m.equals("disable")) {
                mMode = 0;
            } else if (m.equals("decelerate")) {
                mMode = 1;
            } else if (m.equals("uniform")) {
                mMode = 2;
            }

            setManualScroll(TypedAttrUtils.getBoolean(attrs, "manualScroll", true));
            mInertiaX = TypedAttrUtils.getFloat(attrs, "startAngleX", 0.5f);
            mInertiaY = TypedAttrUtils.getFloat(attrs, "startAngleY", 0.5f);
            int light = TypedAttrUtils.getColor(attrs, "lightColor", Color.WHITE).getValue();
            setLightColor(light);
            int dark = TypedAttrUtils.getColor(attrs, "darkColor", Color.BLACK).getValue();
            setDarkColor(dark);
            float p = TypedAttrUtils.getFloat(attrs, "radiusPercent", mRadiusPercent);
            setRadiusPercent(p);
            float s = TypedAttrUtils.getFloat(attrs, "scrollSpeed", 2f);
            setScrollSpeed(s);
        }
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(getContext())
                .get().getAttributes();
        Point point;
        point = new Point(attributes.width, attributes.height);
        int screenWidth = (int) point.getPointX();
        int screenHeight = (int) point.getPointY();
        mMinSize = screenHeight < screenWidth ? screenHeight : screenWidth;
    }

    public void setAutoScrollMode(@Mode int mode) {
        this.mMode = mode;
    }

    @Mode
    public int getAutoScrollMode() {
        return this.mMode;
    }

    public final void setAdapter(TagsAdapter adapter) {
        isGetView = false;

        mAdapter = adapter;
        mAdapter.setOnDataSetChangeListener(this);
        onChange();
    }


    public final void setGetViewAdapter(TagsAdapter adapter) {
        isGetView = true;
        mAdapter = adapter;
        mAdapter.setOnDataSetChangeListener(this);
        onChange();
    }

    public void setManualScroll(boolean manualScroll) {
        this.manualScroll = manualScroll;
    }

    public void setLightColor(int color) {
        float[] argb = new float[4];
        argb[3] = Color.alpha(color) / 1.0f / 0xff;
        argb[0] = red(color) / 1.0f / 0xff;
        argb[1] = green(color) / 1.0f / 0xff;
        argb[2] = blue(color) / 1.0f / 0xff;
        mLightColor = argb.clone();
        onChange();
    }

    public void setDarkColor(int color) {
        float[] argb = new float[4];
        argb[3] = Color.alpha(color) / 1.0f / 0xff;
        argb[0] = red(color) / 1.0f / 0xff;
        argb[1] = green(color) / 1.0f / 0xff;
        argb[2] = blue(color) / 1.0f / 0xff;
        mDarkColor = argb.clone();
        onChange();
    }
    public void setRadiusPercent(float percent) {
        if (percent > 1f || percent < 0f) {
            throw new IllegalArgumentException("percent value not in range 0 to 1");
        } else {
            mRadiusPercent = percent;
            onChange();
        }
    }
    private void initFromAdapter() {
        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                mCenterX = Float.parseFloat(String.valueOf((getRight() - getLeft()) / 2));
                mRadius = Math.min(mCenterX * mRadiusPercent, mCenterY * mRadiusPercent);
                mCenterY = Float.parseFloat(String.valueOf((getBottom() - getTop()) / 2));
                mTagCloud.setRadius((int) mRadius);
                mTagCloud.setTagColorLight(mLightColor);//higher color
                mTagCloud.setTagColorDark(mDarkColor);//lower color
                mTagCloud.clear();
                removeAllComponents();
                for (int i = 0; i < mAdapter.getCount(); i++) {
                    //binding view to each tag
                    Tag tag = new Tag(mAdapter.getPopularity(i));
                    Component view = mAdapter.getView(getContext(), i, TagCloudView.this);
                    tag.bindingView(view);
                    mTagCloud.add(tag);
                    addListener(view, i);
                }
                mTagCloud.setInertia(mInertiaX, mInertiaY);
                mTagCloud.create(true);
                resetChildren();
            }
        }, 1000);
    }

    private void addListener(Component view, final int position) {
        if (mOnTagClickListener != null) {
            view.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component v) {
                    mOnTagClickListener.onItemClick(TagCloudView.this, v, position);
                }
            });
        }
    }

    public void setScrollSpeed(float scrollSpeed) {
        mSpeed = scrollSpeed;
    }

    private void resetChildren() {
        removeAllComponents();
        //必须保证getChildAt(i) == mTagCloud.getTagList().get(i)
        for (Tag tag : mTagCloud.getTagList()) {
            addComponent(tag.getView());
        }
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        super.setEstimatedSize(i, i1);
        int contentWidth = EstimateSpec.getSize(i);
        int contentHeight = EstimateSpec.getSize(i1);
        int widthMode = EstimateSpec.getMode(i);
        int heightMode = EstimateSpec.getMode(i1);
        int dimensionX = widthMode == EstimateSpec.PRECISE ? contentWidth : mMinSize
                - getMarginLeft() - getMarginRight();
        int dimensionY = heightMode == EstimateSpec.PRECISE ? contentHeight : mMinSize
                - getMarginLeft() - getMarginRight();
        setEstimatedSize(dimensionX, dimensionY);
        // 通知子组件进行测量
        measureChildren(dimensionX, dimensionY);
        return true;
    }

    private void measureChildren(int widthEstimatedConfig, int heightEstimatedConfig) {
        for (int idx = 0; idx < getChildCount(); idx++) {
            Component childView = getComponentAt(idx);
            if (childView != null) {
                measureChild(childView, widthEstimatedConfig, heightEstimatedConfig);
            }
        }
    }

    private void measureChild(Component child, int parentWidthMeasureSpec, int parentHeightMeasureSpec) {
        ComponentContainer.LayoutConfig lc = child.getLayoutConfig();
        int childWidthMeasureSpec = EstimateSpec.getChildSizeWithMode(
                lc.width, parentWidthMeasureSpec, EstimateSpec.UNCONSTRAINT);
        int childHeightMeasureSpec = EstimateSpec.getChildSizeWithMode(
                lc.height, parentHeightMeasureSpec, EstimateSpec.UNCONSTRAINT);
        child.estimateSize(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        mHandler.postTask(runnable);
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        mHandler.removeAllEvent();
    }

    @Override
    public boolean onArrange(int l, int t, int r, int b) {
        for (int i = 0; i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            Tag tag = mTagCloud.get(i);
            if (child != null && child.getVisibility() != HIDE) {
                if (isGetView) {
                    Component view = mAdapter.getView(getContext(), i, TagCloudView.this);
                    mAdapter.onThemeColorChanged(view, tag.getColor(), tag.getAlpha());
                } else {
                    mAdapter.onThemeColorChanged(child, tag.getColor(), tag.getAlpha());
                }
                child.setScaleX(tag.getScale());
                child.setScaleY(tag.getScale());
                int left, top;
                left = (int) (mCenterX + tag.getFlatX()) - child.getEstimatedWidth() / 2;
                top = (int) (mCenterY + tag.getFlatY()) - child.getEstimatedHeight() / 2;
                child.arrange(left, top, child.getEstimatedWidth(), child.getEstimatedHeight());
            }
        }
        return true;
    }

    public void reset() {
        mTagCloud.reset();
        resetChildren();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (manualScroll) {
            handleTouchEvent(touchEvent);
        }
        return true;
    }


    private float downX, downY;

    private void handleTouchEvent(TouchEvent e) {
        MmiPoint point = e.getPointerPosition(e.getIndex());
        switch (e.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                downX = point.getX();
                downY = point.getY();
                mIsOnTouch = true;
            case TouchEvent.POINT_MOVE:
                //rotate elements depending on how far the selection point is from center of cloud
                float dx = point.getX() - downX;
                float dy = point.getY() - downY;
                if (isValidMove(dx, dy)) {
                    mInertiaX = (dy / mRadius) * mSpeed * TOUCH_SCALE_FACTOR;
                    mInertiaY = (-dx / mRadius) * mSpeed * TOUCH_SCALE_FACTOR;
                    try {
                        processTouch();
                    } catch (Exception exception) {
                        exception.fillInStackTrace();
                    }
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                mIsOnTouch = false;
                break;
            default:
                break;
        }
    }
    //TODO  固定值
    private boolean isValidMove(float dx, float dy) {
        //满足这个像素距离，可以认为用户在滚动中
        //ComponentContainer
        return (Math.abs(dx) > 27 || Math.abs(dy) > 27);
    }

    private void processTouch() {
        if (mTagCloud != null) {
            mTagCloud.setInertia(mInertiaX, mInertiaY);
            mTagCloud.update();
        }
        resetChildren();
    }

    @Override
    public void onChange() {
        initFromAdapter();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!mIsOnTouch && mMode != MODE_DISABLE) {
                if (mMode == MODE_DECELERATE) {
                    if (mInertiaX > 0.04f) {
                        mInertiaX -= 0.02f;
                    }
                    if (mInertiaY > 0.04f) {
                        mInertiaY -= 0.02f;
                    }
                    if (mInertiaX < -0.04f) {
                        mInertiaX += 0.02f;
                    }
                    if (mInertiaY < -0.04f) {
                        mInertiaY += 0.02f;
                    }
                }
                processTouch();
            }
            mHandler.postTask(this, 16);
        }
    };

    public void setOnTagClickListener(OnTagClickListener listener) {
        mOnTagClickListener = listener;
    }

    public interface OnTagClickListener {
        void onItemClick(ComponentContainer parent, Component view, int position);
    }
    public static int red(int color) {
        return (color & Constants.DEFAULT) >> 16;
    }

    public static int green(int color) {
        return (color & Constants.DEFAULT2) >> 8;
    }


    public static int blue(int color) {
        return color & Constants.DEFAULT3;
    }

    private void debug(String msg) {
        HiLog.error(LOG_LAB, msg);
    }
}
